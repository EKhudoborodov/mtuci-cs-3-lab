# 6.0001 Problem Set 3
#
# The 6.0001 Word Game
# Created by: Kevin Luu <luuk> and Jenna Wiens <jwiens>
#
# Name          : <your name>
# Collaborators : <your collaborators>
# Time spent    : <total time>

import math
import random
import string

VOWELS = 'aeiou'
CONSONANTS = 'bcdfghjklmnpqrstvwxyz'
HAND_SIZE = 7

SCRABBLE_LETTER_VALUES = {
    'a': 1, 'b': 3, 'c': 3, 'd': 2, 'e': 1, 'f': 4, 'g': 2, 'h': 4, 'i': 1, 'j': 8, 'k': 5, 'l': 1, 'm': 3, 'n': 1, 'o': 1, 'p': 3, 'q': 10, 'r': 1, 's': 1, 't': 1, 'u': 1, 'v': 4, 'w': 4, 'x': 8, 'y': 4, 'z': 10
}

# -----------------------------------
# Helper code
# (you don't need to understand this helper code)

WORDLIST_FILENAME = "words.txt"

def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # wordlist: list of strings
    wordlist = []
    for line in inFile:
        wordlist.append(line.strip().lower())
    print("  ", len(wordlist), "words loaded.")
    return wordlist

def get_frequency_dict(sequence):
    """
    Returns a dictionary where the keys are elements of the sequence
    and the values are integer counts, for the number of times that
    an element is repeated in the sequence.

    sequence: string or list
    return: dictionary
    """
    
    # freqs: dictionary (element_type -> int)
    freq = {}
    for x in sequence:
        freq[x] = freq.get(x,0) + 1
    return freq
	

# (end of helper code)
# -----------------------------------

#
# Problem #1: Scoring a word
#
def get_word_score(word, n):
    """
    Returns the score for a word. Assumes the word is a
    valid word.

    You may assume that the input word is always either a string of letters, 
    or the empty string "". You may not assume that the string will only contain 
    lowercase letters, so you will have to handle uppercase and mixed case strings 
    appropriately. 

	The score for a word is the product of two components:

	The first component is the sum of the points for letters in the word.
	The second component is the larger of:
            1, or
            7*wordlen - 3*(n-wordlen), where wordlen is the length of the word
            and n is the hand length when the word was played

	Letters are scored as in Scrabble; A is worth 1, B is
	worth 3, C is worth 3, D is worth 2, E is worth 1, and so on.

    word: string
    n: int >= 0
    returns: int >= 0
    """
    word = word.lower()
    sum = 0
    one = ['a', 'e', 'i', 'l', 'n', 'o', 'r', 's', 't', 'u']
    two = ['d', 'g']
    three = ['b', 'c', 'm', 'p']
    four = ['f', 'h', 'v', 'w', 'y']
    five = ['k']
    eight = ['j', 'x']
    #ten = ['q', 'z']
    for char in word:
        char_found = 0
        for i in range (len(one)):
            if char == one[i]:
                sum += 1
                char_found = 1
                #print('1')
                break
        if char_found == 1:
            continue
        for i in range (len(two)):
            if char == two[i]:
                sum += 2
                char_found = 1
                #print('2')
                break
        if char_found == 1:
            continue
        for i in range(len(three)):
            if char == three[i]:
                sum += 3
                char_found = 1
                #print('3')
                break
        if char_found == 1:
            continue
        for i in range(len(four)):
            if char == four[i]:
                sum += 4
                char_found = 1
                #print('4')
                break
        if char_found == 1:
            continue
        for i in range(len(five)):
            if char == five[i]:
                sum += 5
                char_found = 1
                #print('5')
                break
        if char_found == 1:
            continue
        for i in range(len(eight)):
            if char == eight[i]:
                sum += 8
                char_found = 1
                #print('8')
                break
        if char_found == 1:
            continue
        elif char != '*':
            sum += 10
            #print('10')

    if (7*len(word) - 3*(n-len(word))) > 0:
        score = sum * (7*len(word)-3*(n-len(word)))
    else:
        score = sum
    return score


#
# Make sure you understand how this function works and what it does!
#
def display_hand(hand):
    """
    Displays the letters currently in the hand.

    For example:
       display_hand({'a':1, 'x':2, 'l':3, 'e':1})
    Should print out something like:
       a x x l l l e
    The order of the letters is unimportant.

    hand: dictionary (string -> int)
    """
    letters = ''
    #chars = ''
    for letter in hand.keys():
        for j in range(hand[letter]):
             letters += letter + " "
             #chars += letter
             #print(letter, end=' ')      # print all on the same line
    print(f"Current hand: {letters}")
    #return chars

#
# Make sure you understand how this function works and what it does!
# You will need to modify this for Problem #4.
#
def deal_hand(n):
    """
    Returns a random hand containing n lowercase letters.
    ceil(n/3) letters in the hand should be VOWELS (note,
    ceil(n/3) means the smallest integer not less than n/3).

    Hands are represented as dictionaries. The keys are
    letters and the values are the number of times the
    particular letter is repeated in that hand.

    n: int >= 0
    returns: dictionary (string -> int)
    """
    
    hand = {}
    num_vowels = int(math.ceil(n / 3))

    for i in range(num_vowels):
        if i == 0:
            x = '*'
            hand[x] = hand.get(x, 0) + 1
        else:
            x = random.choice(VOWELS)
            hand[x] = hand.get(x, 0) + 1
    
    for i in range(num_vowels, n):    
        x = random.choice(CONSONANTS)
        hand[x] = hand.get(x, 0) + 1
    
    return hand

#
# Problem #2: Update a hand by removing letters
#
def update_hand(hand, word):
    """
    Does NOT assume that hand contains every letter in word at least as
    many times as the letter appears in word. Letters in word that don't
    appear in hand should be ignored. Letters that appear in word more times
    than in hand should never result in a negative count; instead, set the
    count in the returned hand to 0 (or remove the letter from the
    dictionary, depending on how your code is structured). 

    Updates the hand: uses up the letters in the given word
    and returns the new hand, without those letters in it.

    Has no side effects: does not modify hand.

    word: string
    hand: dictionary (string -> int)    
    returns: dictionary (string -> int)
    """
    word = word.lower()
    new_hand = {}
    for letter in hand.keys():
        for j in range(hand[letter]):
            letter_not_found = 1
            new_word = ''
            #i = 1
            for char in word:
                if ((char == letter) & (letter_not_found == 1)):
                    letter_not_found = 0
                    #i = 0
                else:
                    new_word += char
            word = new_word
            if letter_not_found == 1:
                new_hand[letter] = new_hand.get(letter, 0) + 1
    return new_hand
#
# Problem #3: Test word validity
#

def check_word(word, hand):
    #print(word, ' ', hand)
    for char in word:
        letter_not_found = 1
        new_hand = {}
        #i = 1
        for letter in hand.keys():
            for j in range(hand[letter]):
                if ((char == letter) & (letter_not_found == 1)):
                    letter_not_found = 0
                    #i = 0
                else:
                    # new_word += char
                    new_hand[letter] = new_hand.get(letter, 0) + 1
        # word = new_word
        hand = new_hand
        if letter_not_found == 1:
            #print(hand)
            return False
    return True

def is_valid_word(word, hand, word_list):
    """
    Returns True if word is in the word_list and is entirely
    composed of letters in the hand. Otherwise, returns False.
    Does not mutate hand or word_list.
   
    word: string
    hand: dictionary (string -> int)
    word_list: list of lowercase strings
    returns: boolean
    """
    vowels = 'aeiou'
    wild_on = 0
    word_not_found = 1
    word = word.lower()
    for char in word:
        if char == '*':
            wild_on = 1
            break
    for s in word_list:
        if wild_on == 1:
            wrong = 0
            if len(s) == len(word):
                for k in range(len(word)):
                    if ((word[k] != s[k]) & (word[k] != '*')):
                        wrong = 1
                        break
                    elif word[k] == '*':
                        not_vowel = 1
                        for vowel in vowels:
                            if s[k] == vowel:
                                not_vowel = 0
                                a = vowel
                                #print(a)
                        if not_vowel == 1:
                            wrong = 1
                            break
            else:
                wrong = 1
            if wrong == 0:
                return check_word(word, hand)
        elif s == word:
            return check_word(word, hand)
    if word_not_found == 1:
        return False



#
# Problem #5: Playing a hand
#
def calculate_handlen(hand):
    """ 
    Returns the length (number of letters) in the current hand.
    
    hand: dictionary (string-> int)
    returns: integer
    """
    n = 0
    for letter in hand.keys():
        for j in range(hand[letter]):
            n += 1
    return n

def play_hand(hand, word_list):

    """
    Allows the user to play the given hand, as follows:

    * The hand is displayed.
    
    * The user may input a word.

    * When any word is entered (valid or invalid), it uses up letters
      from the hand.

    * An invalid word is rejected, and a message is displayed asking
      the user to choose another word.

    * After every valid word: the score for that word is displayed,
      the remaining letters in the hand are displayed, and the user
      is asked to input another word.

    * The sum of the word scores is displayed when the hand finishes.

    * The hand finishes when there are no more unused letters.
      The user can also finish playing the hand by inputing two 
      exclamation points (the string '!!') instead of a word.

      hand: dictionary (string -> int)
      word_list: list of lowercase strings
      returns: the total score for the hand
      
    """
    game = 1
    score = 0
    while game == 1:
        lenght = calculate_handlen(hand)
        if lenght == 0:
            print(f"Ran out of letters.\nTotal score for this hand: {score} points\n----------")
            break
        display_hand(hand)
        word = input("Enter word, or '!!' to indicate that you are finished:")
        if word == '!!':
            print(f"Total score for this hand: {score} points\n----------")
            game = 0
        elif is_valid_word(word, hand, word_list) == True:
            score += get_word_score(word, lenght)
            print (f"'{word}' earned {get_word_score(word, lenght)}. Total points: {score} points")
            print()
            hand = update_hand(hand, word)
        else:
            print("That is not a valid word. Please choose another word.")
            print()
            hand = update_hand(hand, word)
    return score
    # BEGIN PSEUDOCODE <-- Remove this comment when you implement this function
    # Keep track of the total score
    
    # As long as there are still letters left in the hand:
    
        # Display the hand
        
        # Ask user for input
        
        # If the input is two exclamation points:
        
            # End the game (break out of the loop)

            
        # Otherwise (the input is not two exclamation points):

            # If the word is valid:

                # Tell the user how many points the word earned,
                # and the updated total score

            # Otherwise (the word is not valid):
                # Reject invalid word (print a message)
                
            # update the user's hand by removing the letters of their inputted word
            

    # Game is over (user entered '!!' or ran out of letters),
    # so tell user the total score

    # Return the total score as result of function



#
# Problem #6: Playing a game
# 


#
# procedure you will use to substitute a letter in a hand
#

def substitute_hand(hand, letter):
    """ 
    Allow the user to replace all copies of one letter in the hand (chosen by user)
    with a new letter chosen from the VOWELS and CONSONANTS at random. The new letter
    should be different from user's choice, and should not be any of the letters
    already in the hand.

    If user provide a letter not in the hand, the hand should be the same.

    Has no side effects: does not mutate hand.

    For example:
        substitute_hand({'h':1, 'e':1, 'l':2, 'o':1}, 'l')
    might return:
        {'h':1, 'e':1, 'o':1, 'x':2} -> if the new letter is 'x'
    The new letter should not be 'h', 'e', 'l', or 'o' since those letters were
    already in the hand.
    
    hand: dictionary (string -> int)
    letter: string
    returns: dictionary (string -> int)
    """
    ABC = 'abcdefghijklmnopqrstuvwxyz'
    string =''
    x = random.choice(ABC)
    for chars in ABC:
        for char in hand.keys():
            if chars != char:
                string += chars
    ABC = string
    for char in hand.keys():
        if x == char:
            x = random.choice(ABC)
    new_hand = {}
    for char in hand.keys():
        for j in range(hand[char]):
            if char == letter:
                #x = random.choice(ABC)
                new_hand[x] = new_hand.get(x, 0) + 1
            else:
                new_hand[char] = new_hand.get(char, 0) + 1
    if hand == new_hand:
        print()
        print(f"There is no '{letter}' in current hand.")
        letter = input("Which letter would you like to substitute?")
        new_hand = substitute_hand(hand, letter)
    return new_hand

       
    
def play_game(word_list):
    """
    Allow the user to play a series of hands

    * Asks the user to input a total number of hands

    * Accumulates the score for each hand into a total score for the 
      entire series
 
    * For each hand, before playing, ask the user if they want to substitute
      one letter for another. If the user inputs 'yes', prompt them for their
      desired letter. This can only be done once during the game. Once the
      substitue option is used, the user should not be asked if they want to
      substitute letters in the future.

    * For each hand, ask the user if they would like to replay the hand.
      If the user inputs 'yes', they will replay the hand and keep 
      the better of the two scores for that hand.  This can only be done once 
      during the game. Once the replay option is used, the user should not
      be asked if they want to replay future hands. Replaying the hand does
      not count as one of the total number of hands the user initially
      wanted to play.

            * Note: if you replay a hand, you do not get the option to substitute
                    a letter - you must play whatever hand you just had.
      
    * Returns the total score for the series of hands

    word_list: list of lowercase strings
    """
    available_replay = 1
    sum = 0
    num = int(input("Enter total number of hands:"))
    for i in range(num):
        hand = deal_hand(HAND_SIZE)
        """
        if i == 0:
            hand = {'a': 1, 'c': 1, 'i': 1, '*': 1, 'p': 1, 'r': 1, 't': 1}
        else:
            hand = {'d': 2, '*': 1, 'l': 1, 'o': 1, 'u': 1, 't': 1}
        """
        display_hand(hand)
        substitute = input("Would you like to substitute a letter?")
        if substitute == 'yes':
            letter = input("Which letter would you like to replace:")
            hand = substitute_hand(hand, letter)
            #display_hand(hand)
        print()
        score = play_hand(hand, word_list)
        if available_replay == 1:
            replay = input("Would you like to replay the hand?")
            if replay == 'yes':
                score = play_hand(hand, word_list)
            else:
                print()
        sum += score
    print(f"Total score over all hands: {sum}")
    


#
# Build data structures used for entire session and play game
# Do not remove the "if __name__ == '__main__':" line - this code is executed
# when the program is run directly, instead of through an import statement
#
if __name__ == '__main__':
    word_list = load_words()

    play_game(word_list)
    #hand = {'a': 1, 'j': 1, 'e': 1, 'f': 1, '*': 1, 'r': 1, 'x': 1}
    #print(substitute_hand(hand, 'a'))
    #hand = {'a': 1, 'c': 1, 'f': 1, 'i': 1, '*': 1, 't': 1, 'x': 1}
    #play_hand(hand, word_list)
    #print(is_valid_word('h*ney', hand, word_list))
    #hand = deal_hand(6)
    #hand = {'e': 1, 'v': 2, 'n': 1, 'i': 1, 'l': 2}
    #print(is_valid_word('evil',hand,word_list))
    #hand = {'j': 2, 'o': 1, 'l': 1, 'w': 1, 'n': 2}
    #print(update_hand(hand, 'jolly'))
    #hand = {'a': 1, 'q': 1, 'l': 2, 'm': 1, 'u': 1, 'i': 1}
    #display_hand(hand)
    #print(calculate_handlen(hand))
    #print(update_hand(hand, 'quail'))
    #print(deal_hand(6))
    #display_hand({'w':1, 'e': 2, 'd': 1})
    #print(get_word_score('h*ney', 7))
